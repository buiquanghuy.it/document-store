FROM docker:latest

RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community nodejs-npm

# install simple http server for serving static content
RUN npm install -g http-server

RUN npm i -g heroku

# make the 'app' folder the current working directory
WORKDIR /app

# install project dependencies
RUN npm install

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .

# build app for production with minification
RUN npm run build